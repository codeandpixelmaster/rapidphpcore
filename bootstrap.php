<?php
/**
 * bootstrap.php
 * Autoload framework components
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 28-Mar-2015
 * @package RapidPHPMe Core
 **/

function rapid_autoloader( $class )
{
    $thisdir = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'Rapid';
    
    if( file_exists( $thisdir . '/' . $class . '.php' ) )
    {
        include( $thisdir . '/' . $class . '.php' );
    }
    elseif( file_exists( $thisdir . '/functions/' . $class . '.php' ) )
    {
        include( $thisdir . '/functions/' . $class . '.php' );
    }
	elseif( file_exists( $thisdir . '/helpers/' . $class . '.php' ) )
	{
		include( $thisdir . '/helpers/' . $class . '.php' );
	}
}
spl_autoload_register( 'rapid_autoloader' );
