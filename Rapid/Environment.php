<?php
/**
 * Environment.php
 *
 * @author Bennett Stone
 * @version 1.1
 * @date 26-Mar-2015
 * @updated 04-Jul-2016
 * @package RapidPHPMe Core
 **/

class Environment {
    
    public function __construct()
    {
        //Add environment checker
        require_once( ROOT . SEP .'application' . SEP . 'config'. SEP . 'environments.php' );

        $env_file = ROOT . '/.env.'. ENVIRONMENT . '.php';

        if( file_exists( $env_file ) )
        {
            putenv( 'APP_ENV=' . ENVIRONMENT );
            try {
                ( new \Dotenv\Dotenv( ROOT, '.env.'. getenv( 'APP_ENV' ).'.php' ) )->overload();
            } catch (\Dotenv\Exception\InvalidPathException $e) {
                //Do nothing.  No .env files set
            }
        }
    }
    //end construct


    /**
     * @src http://stackoverflow.com/a/10473026
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith( $haystack, $needle )
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }


    /**
     * @src http://stackoverflow.com/a/10473026
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endsWith( $haystack, $needle )
    {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

}
//end class Environment