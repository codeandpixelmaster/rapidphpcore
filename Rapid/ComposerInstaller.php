<?php
/**
 * Composer installer to handle after update and after install events
 * Copies template files, generates views and models, etc...
 *
 * @author Bennett Stone
 * @version 1.1
 * @date 30-Jun-2016
 * @package RapidPHPMe Core
 **/

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class ComposerInstaller
{
	private static $config_path = 'application/config/';

	private static $rocketeer_config = '.rocketeer';

	private static $rocketeer_replacements = array(

		'config.php' => array(
			'APPLICATION_NAME' => 'sitename',
			'CONNECTION_HOST' => 'host',
			'CONNECTION_USERNAME' => 'username',
			'CONNECTION_PASSWORD' => 'password',
			'EMAIL_LIST' => 'emails',
			'WASP_KEY' => 'waspkey',
		),

		'remote.php' => array(
			'ROOT_DIRECTORY' => 'root_directory',
		),

		'scm.php' => array(
			'REPOSITORY' => 'repository',
			'USERNAME' => 'repository_user',
			'PASSWORD' => 'repository_password',
		),
	);

	private static $sitename = 'RapidPHP';

	private static $generate_templates = false;

	private static $waspkey = '';

	private static $host = '';

	private static $username = '';

	private static $password = '';

	private static $emails = array();

	private static $root_directory = '';

	private static $repository = '';

	private static $repository_user = '';

	private static $repository_password = '';

	private static $copy_templates = array(

		'function.php' => 'application/controllers/controller-example-function.php',

		'modelClass.php' => 'application/controllers/Example.php',

		'view.php' => 'application/views/example-view.php',
	);

	private static $reserved = array(
		'__halt_compiler',
		'abstract',
		'and',
		'array',
		'as',
		'break',
		'callable',
		'case',
		'catch',
		'class',
		'clone',
		'const',
		'continue',
		'declare',
		'default',
		'die',
		'do',
		'echo',
		'else',
		'elseif',
		'empty',
		'enddeclare',
		'endfor',
		'endforeach',
		'endif',
		'endswitch',
		'endwhile',
		'eval',
		'exit',
		'extends',
		'final',
		'for',
		'foreach',
		'function',
		'global',
		'goto',
		'if',
		'implements',
		'include',
		'include_once',
		'instanceof',
		'insteadof',
		'interface',
		'isset',
		'list',
		'namespace',
		'new',
		'or',
		'print',
		'private',
		'protected',
		'public',
		'require',
		'require_once',
		'return',
		'static',
		'switch',
		'throw',
		'trait',
		'try',
		'unset',
		'use',
		'var',
		'while',
		'xor',
		'app',
		'control',
		'configloader',
		'environment',
		'helper',
		'loader',
		'models',
		'options',
		'router',
		'sessions',
		'statusresponse',
		'storage',
		'uriparser',
		'composerinstaller',
	);


	/**
	 * Function called by composer
	 * @param Event $event
	 * @return bool
	 */
	public static function after_install( Event $event )
	{
		// provides access to the current Composer instance
		$io = $event->getIO();
		// run any post install tasks here
		$file = 'application/config/config.php';

		//Copy the env file
		copy( dirname( __FILE__ ) . '/template/.env.example.php', './.env.example.php' );

		//Get the sitename
		$write = false;
		try {
			$data = $io->ask( "\n\n<question>What is the site name?</question>\n" );
			if( !empty( $data ) )
			{
				self::$sitename = trim( $data );
				$write = true;
			}
		} catch( Exception $e ) {}

		//Get the wasp project API key if any
		try {
			$key = $io->ask( "\n\nEnter your Wasp.io API key (if you have one):\n" );
			if( !empty( $key ) )
			{
				self::$waspkey = trim( $key );
				$write = true;
			}
		} catch( Exception $e ) {}

		//Check to see if we should set up rocketeer
		$rocket = false;
		try {
			$rocket = $io->askConfirmation( "\n\n<question>Use rocketeer?</question>\n" );
			if( !empty( $rocket ) )
			{
				try {
					$host = $io->ask( "\nWhat is the SSH hostname?:\n" );
					if( !empty( $host ) )
					{
						self::$host = trim( $host );
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$username = $io->ask( "\nWhat is the SSH username?:\n" );
					if( !empty( $username ) )
					{
						self::$username = trim( $username );
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$pass = $io->ask( "\nWhat is the SSH password?:\n" );
					if( !empty( $pass ) )
					{
						self::$password = trim( $pass );
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$root = $io->ask( "\nWhat is the root deployment directory (no trailing slash)?:\n" );
					if( !empty( $root ) )
					{
						self::$root_directory = $root;
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$git = $io->ask( "\nWhat is the SSH/HTTPS address to your repository?:\n" );
					if( !empty( $git ) )
					{
						self::$repository = $git;
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$gituser = $io->ask( "\nWhat is repository username (if not public)?:\n" );
					if( !empty( $gituser ) )
					{
						self::$repository_user = $gituser;
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$gitpass = $io->ask( "\nWhat is repository password (if not public)?:\n" );
					if( !empty( $gitpass ) )
					{
						self::$repository_password = $gitpass;
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				try {
					$emails = $io->ask( "\nWhat email addresses should be notified for deployments (comma separated list)?\n" );
					if( !empty( $emails ) )
					{
						//$emails = explode( ',', $emails );
						self::$emails = $emails; //implode( ', ', array_filter( $emails ) );
						$rocket = true;
					}
				} catch( Exception $e ) {
					$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
				}

				if( $rocket )
				{
					self::handle_rocketeer( $io );
				}
			}
		} catch( Exception $e ) {
			$io->write("<error>". $e->getMessage() . ' on '. $e->getLine() ."</error>" );
		}

		//Determine if the user wants to generate default controller/view files
		self::generate_prompt( $event );

		//Handle the actual file and generation business
		$over = ( $write === true ) ? self::overwrite( $file ) : true;
		if( $over )
		{
			$io->write( "<info>Successfully created encryption key in config.php</info>\n" );
			if( !empty( self::$waspkey ) )
			{
				$io->write( "<info>Successfully added Wasp.io API key</info>\n" );
			}
		}
		else
		{
			$io->write( "<error>Unable to create encryption key to ". $file."</error>\n" );
		}

		if( self::$generate_templates === true )
		{
			$made = self::make_templates();
			if( $made > 0 )
			{
				$io->write( "<info>Created '. $made .' example files</info>\n" );
			}
		}

		return true;
	}
	//end after_install()


	/**
	 * Initialize the command line prompts
	 * @param Event $event
	 */
	public static function after_update( Event $event )
	{
		$updated = self::generate_prompt( $event );

		$io = $event->getIO();

		$message = '';
		if( !empty( $updated['generated'] ) )
		{
			foreach( $updated['generated'] as $k )
			{
				foreach( $k as $n => $t )
				{
					$message .= "\n". $t;
				}
			}
			$io->write( "\n<info>Generated:\n $message</info>\n" );
		}

		$message = '';
		if( !empty( $updated['skipped'] ) )
		{
			foreach( $updated['skipped'] as $k )
			{
				$message .= "\n". $k;
			}
			$io->write( "\n<error>Skipped due to reserved keywords:\n $message</error>\n" );
		}

	}
	//end after_update()


	/**
	 * Generate terminal prompts for user
	 *
	 * @param Event $event
	 * @return array
	 */
	private static function generate_prompt( Event $event )
	{
		$io = $event->getIO();
		try {
			$cnt = $io->askConfirmation( "\n\n<question>Would you like to auto-generate example controller and view files? yes/no</question>\n", false );

			$files_generated = array();
			$skipped = array();
			if( !empty( $cnt ) )
			{
				$controllernames = $io->ask( "\n<comment>Enter a list of Classes and their respective functions.  For example:\n\nAdmin : login | manage_users, Users : settings | logout\nTo generate secure access files, suffix the classname with {secure}...\nTest{secure} : one-page | two-page\nTo generate admin access only pages, suffix classnames with {admin}:\nAnotherTest{admin} : third-page</comment>\n\n" );
				if( empty( $controllernames ) )
				{
					self::$generate_templates = $cnt;
				}
				else
				{
					$user = isset( $_SERVER['USER'] ) ? $_SERVER['USER'] : '';
					$inputs = explode( ',', $controllernames );
					foreach( $inputs as $i )
					{
						$params = explode( ':', $i );
						//Initial copy of the method name to check for the secure param
						$clean_method = trim( $params[0] );
						//Lowercase, trim, and capitalize the first letter for the classname
						$method = trim( ucwords( strtolower( $params[0] ) ) );
						$method = trim( str_replace( array( '{secure}', '{admin}' ), '', $params[0] ) );

						//If the method name is in the reserved keywords list, just throw an error, we can't use anything inside it
						if( in_array( strtolower( $method ), self::$reserved ) )
						{
							$skipped[] = $method;
							continue;
						}

						//Default the secure to false
						$secure = 1;
						//Find out if [secure] has been added to the method name
						if( preg_match( "/{secure}/i", $clean_method ) )
						{
							$secure = 2;
						}
						elseif( preg_match( "/{admin}/i", $clean_method ) )
						{
							$secure = 3;
						}

						$functions = array();

						$funct = explode( '|', $params[1] );
						foreach( $funct as $f )
						{
							$f = trim( $f );
							$f = trim( strtolower( str_replace( array( '-', ' ' ), '_', $f ) ) );

							//If this is a reserver php word (http://php.net/manual/en/reserved.keywords.php), skip it
							if( in_array( $f, self::$reserved ) )
							{
								$skipped[] = $f;
								continue;
							}
							else
							{
								$functions[] = $f;
							}
						} //end foreach $functions

						$new_files = self::make( $method, $functions, $secure, $user );
						$files_generated[] = $new_files;

					} //end foreach $inputs

				} //end !empty $controllernames

			}

			return array(
				'generated' => $files_generated,
				'skipped' => $skipped,
			);

		} catch( Exception $e ) {}
	}


	/**
	 * Generate secure encrption key
	 *
	 * @return string
	 */
	private static function generate_key()
	{
		$randkey = sha1( microtime( true ).mt_rand( 10000, 90000 ) );
		$randkey = substr( md5( mt_rand() ), 0, 32 );
		return $randkey;
	}


	/**
	 * Overwrite existing vars in template keys
	 * @param $file
	 * @return bool
	 */
	private static function overwrite( $file )
	{
		if( file_exists( $file ) )
		{
			$contents = file_get_contents( $file );
			$search = array(
				'SITE_NAME',
				'ENCRYPTION_KEY',
				'WASP_API_KEY',
			);
			$replace = array(
				self::$sitename,
				self::generate_key(),
				self::$waspkey,
			);
			$data = str_replace( $search, $replace, $contents );
			if( file_put_contents( $file, $data ) )
			{
				return true;
			}
		}
		return false;
	}


	/**
	 * Handle rocketeer configuration files
	 * @return bool|int
	 */
	private static function handle_rocketeer( $io )
	{
		if( !is_dir( self::$rocketeer_config ) )
		{
			$io->write( "<error>Rocketeer configuration directory does not exist</error>\n" );
			return false;
		}

		$modified = 0;
		foreach( self::$rocketeer_replacements as $file => $replacement )
		{
			$file = self::$rocketeer_config . DIRECTORY_SEPARATOR . $file;
			if( !file_exists( $file ) )
			{
				$io->write( "<error>Rocketeer configuration file: {$file} does not exist</error>\n" );
				continue;
			}

			$io->write( "<info>Processing file: {$file}</info>\n" );
			//$io->write( "<info>".$contents."</info>\n" );
			$handle = array();
			foreach( $replacement as $key => $value )
			{
				$newval = self::get( $value );

				if( $newval )
				{
					$handle[$key] = $newval;
				}
			}

			if( empty( $handle ) )
			{
				$io->write( "<error>Nothing to replace in {$file}</error>\n" );
				continue;
			}

			$search = array_keys( $handle );
			$replace = array_values( $handle );
			$data = str_replace( $search, $replace, file_get_contents( $file ) );
			//$io->write( "<info>".$data."</info>\n" );
			if( file_put_contents( $file, $data ) )
			{
				$modified++;
			}

		}
		return $modified;
	}

	/**
	 * @param $var
	 * @return mixed
	 */
	private static function get( $var )
	{
		return self::$$var;
	}


	/**
	 * Make new Controller and view files
	 *
	 * @param $class
	 * @param array $functions
	 * @param int $secure
	 * @param string $user
	 * @return array
	 */
	private static function make( $class, $functions = array(), $secure = 1, $user = '' )
	{
		$files_made = array();

		$new_name = $class .'.php';
		$new_path = 'application/controllers/';

		$view_path = 'application/views/';

		//Inner directories for views
		$view_folder = trim( strtolower( $class ) ) .'/';

		//Base view template
		$view_template = dirname( __FILE__ ) . '/template/view.php';

		$function_file = file_get_contents( dirname( __FILE__ ) . '/template/inner-function.php' );

		//Assign the classfile conditionally to use secure with construct vs. normal
		switch( $secure )
		{
		case 1:
		default:
			$class_file = dirname( __FILE__ ) . '/template/outer-class.php';
			break;
		case 2:
			$class_file = dirname( __FILE__ ) . '/template/outer-class-secure.php';
			break;
		case 3:
			$class_file = dirname( __FILE__ ) . '/template/outer-class-admin.php';
			break;
		}


		$class_file = str_replace(
			array(
				'{{username}}',
				'{{method_name}}',
				'{{date}}'
			),
			array(
				$user,
				$class,
				date( 'd-M-Y' )
			),
			file_get_contents( $class_file )
		);

		$inner_functions = '//FUNCTIONS_GO_HERE';
		if( !empty( $functions ) )
		{
			$inner_functions = '';
			foreach( $functions as $f )
			{
				$clean_f = str_replace( array( '_', ' ' ), '-', trim( strtolower( $f ) ) );
				$f = str_replace( array( '-', ' ' ), '_', trim( strtolower( $f ) ) );
				$inner_functions .= str_replace(
					array(
						'{{function_name}}',
						'{{function_name_normal}}',
					),
					array(
						$f,
						$clean_f,
					),
					$function_file
				);

				//Make the corresponding view folder if it isn't already there
				$view_directory = $view_path . $view_folder;
				if( !is_dir( $view_directory ) )
				{
					mkdir( $view_directory );
				}
				//Make the corresponding view templates if they aren't already there
				$new_view = $view_directory . $clean_f .'.php';
				if( !file_exists( $new_view ) )
				{
					if( copy( $view_template, $new_view ) )
					{
						$files_made[] = $new_view;
					}
				}

				//Add the route to the /application/config/routes.php file for easier access
				self::add_route( $class, $f, $clean_f );

			}
			$class_file = str_replace( '//FUNCTIONS_GO_HERE', $inner_functions, $class_file );
		}

		if( file_put_contents( $new_path . $new_name, $class_file ) )
		{
			$files_made[] = $new_path . $new_name;
		}

		//Return the list of files generated
		return $files_made;

	}
	//end function make


	/**
	 * Make new copied template files
	 *
	 * @return int
	 */
	private static function make_templates()
	{
		$copied = 0;
		foreach( self::$copy_templates as $file => $copied_file )
		{
			$file = dirname( __FILE__ ) . '/template/'. $file;
			if( !file_exists( $copied_file ) )
			{
				if( copy( $file, $copied_file ) )
				{
					$copied++;
				}
			}
		}
		return $copied;
	}


	/**
	 * Add a route to the /application/config/routes.php file
	 *
	 * @param $method
	 * @param $function
	 * @param $clean_function
	 * @return bool
	 */
	private static function add_route( $method, $function, $clean_function )
	{
		$routefile = self::$config_path . 'routes.php';
		if( !file_exists( $routefile ) )
		{
			return false;
		}
		$method_lower = strtolower( $method );

		$string = PHP_EOL;
		$string .= '//Accessible by going to: yoursite.com/'. $method_lower .'/'. $clean_function . PHP_EOL;
		$string .= "Router::set( '{$method}/{$function}', array( 
    '{$method_lower}', 
    '{$clean_function}' 
) );" . PHP_EOL;

		file_put_contents( $routefile, $string, FILE_APPEND );
	}


	/**
	 * Composer checks require-dev dependencies even when you set --no-dev,
	 * causing Travis/CI to raise exception if the requirements for a dev package are not meet
	 * So we make this hack to fix composer "expected behaviour".
	 * Src: https://github.com/composer/composer/issues/5523#issuecomment-233280813
	 */
	public static function fixComposer( Event $event )
	{
		if( $event->isDevMode() )
		{
			return;
		}

		$event->getComposer()->getPackage()->setDevRequires([]);

		return $event;
	}

}