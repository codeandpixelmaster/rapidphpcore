<?php
/**
 * {{method_name}}.php
 *
 * @author {{username}}
 * @version 1.0
 * @date {{date}}
 * @package RapidPHP Templates
 **/

class {{method_name}} extends Control {


	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	//Load your default actions here, or remove if not used
	public function index()
	{

	}
    
    //FUNCTIONS_GO_HERE
    
} //end {{method_name}}