<?php
/**
 * {{method_name}}.php
 *
 * @author {{username}}
 * @version 1.0
 * @date {{date}}
 * @package RapidPHP Templates
 **/

class {{method_name}} extends Control {


	public function __construct()
	{
		parent::__construct();
		$this->users->page_protect();
	}

	//FUNCTIONS_GO_HERE

} //end {{method_name}}