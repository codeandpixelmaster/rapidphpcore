<?php
/**
 * modelClass.php
 * Used by ComposerInstaller.php to make new file
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 28-Mar-2015
 * @package RapidPHP Templates
 **/

class Example extends Control {


    public function __construct()
    {
        parent::__construct();
    }
    
    //This will be accessible at yoursite.com/example/about
    public function about()
    {
        $this->view( 'example-view', array( 'title' => 'Viewed with about method' ) );
    }
    
    //This would also be accessible, even though prefixed with "controller_"
    //at yousite.com/example/contact
    public function controller_contact()
    {
        $this->view( 'example-view', array( 'title' => 'Viewed using controller_ method' ) );
    }
    
    //You can also name it controller_[class]_[method] and it will work
    public function controller_example_home()
    {
        $this->view( 'example-view', array( 'title' => 'Viewed with complex function dec.' ) );
    }
    
} //end Example