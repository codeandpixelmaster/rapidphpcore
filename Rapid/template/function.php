<?php
/**
 * function.php
 * Used by ComposerInstaller.php to make new example function
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 28-Mar-2015
 * @package RapidPHP Templates
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

//This will be accessible at yoursite.com/more-information
function controller_more_information()
{
    //Load /views/layouts/header.php
    //view( 'header' );
    
    //Load /views/contact.php
    view( 'example-view', array( 'title' => 'Need more information?' ) );
    
    //Load /views/layouts/footer.php
    //view( 'footer' );   
}