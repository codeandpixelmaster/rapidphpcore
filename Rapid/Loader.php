<?php
/**
 * Loader.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

class Loader {
        
    private $addl_includes = array(
        'view-helper.php', 
        'template-output.php', 
        'timer.class.php', 
    );
    
    private $search_paths = array(
        \RAPID_CORE, 
        \RAPID_HELPERS, 
        \CORE, 
        \HELPERS, 
        \USER_HELPERS,
		\USER_MODELS,
        \PLUGINS, 
    );
    
    public function __construct()
    {

        spl_autoload_register( array( $this, 'core_autoload' ) );
        
        $this->autoload_classes();
        
        $this->load_functions();
    }
    //end construct
    
    
    /**
     * Iterate through required classes, and additional classes specified by user
     */
    private function autoload_classes()
    {
        $autoload = array();
        
        //Additional files to require
        if( !empty( $this->addl_includes ) )
        {
            foreach( $this->addl_includes as $addl )
            {
                $autoload[] = RAPID_HELPERS . $addl;
            }
        }
        
        //Retrieve the contents of the autoload array as defined in application/config/autoload.php
        $autoload = array_merge( $autoload, Options::get_autoload() );
        $autoload = array_unique( $autoload );

        $autoload_references = array_unique(
            include_once( RAPID_HELPERS . 'autoload-references.php' )
        );
        

        if( !empty( $autoload ) && is_array( $autoload ) )
        {
            foreach( $autoload as $load )
            {
                //Error supression may be enabled by prefixing include with @
                $load = trim( $load );

                //If the user initiated autoload with a name; list exists in /config/autoload.php
                if( array_key_exists( $load, $autoload_references ) )
                {
                    $load = $autoload_references[$load];
                }

                if( !empty( $load ) && file_exists( $load ) )
                {
                    require_once( $load );   
                }
            }
        }
    }
    //end autoload_classes
    
    
    /**
     * SPL autoload registrar
     * @access public
     * @param string $class
     * @return file
     */
    public function core_autoload( $class )
    {
        $file = $class . '.class.php';
        $subfile = $class .'.php';

        foreach( $this->search_paths as $loc )
        {
            if( file_exists( $loc . $file  ) )
            {
                include_once( $loc . $file );
                break;
            }
            elseif( file_exists( $loc . $subfile ) )
            {
                include_once( $loc . $subfile );
                break;
            }
        }
    }
    //end core_autoload
    
    
    private function load_functions()
    {
        $files = glob( RAPID_FUNCTIONS . '*', GLOB_NOSORT );

        //Remove system files
        $files = array_diff( $files, array( '..', '.' ) );

        //If the array isn't empty, loop it
        if( !empty( $files ) )
        {
            foreach( $files as $f )
            {
                //If it isn't a directory, add to the list
                if( !is_dir( $f ) )
                {
                    require_once( $f );
                }
            }   
        }
    }
    //end load_functions()

}
//end class Loader