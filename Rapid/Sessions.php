<?php
/**
 * Sessions.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

class Sessions {
    
    private static $inst = null;
    
    public function __construct()
    {
        /**
         * Altered to run check: http://stackoverflow.com/a/6010453
         * Force sitewide gzip if the server supports it
         * Gzip directives are also located in .htaccess
         */
        if( !in_array( 'ob_gzhandler', ob_list_handlers() ) )
        {
            ob_start( 'ob_gzhandler' );
        }
        else
        {
            ob_start();
        }
        
        //Auto start the session if this class gets initiated at all
        $this->force_session();
    }
    
    public static function init()
    {
        if( self::$inst == null )
        {
            self::$inst = new Sessions();
        }
        return self::$inst;
    }
    //end init()
    
    /**
     * Function to reliably start sessions
     * @access public
     * @param none
     * @return none
     */
    public function force_session()
    {
        if( ( !session_id() || !strlen( session_id() ) ) )
        {
            session_start();
        }
        else
        {
            //Provide a 5% chance of the session being regenerated for security
            if( rand( 1, 100 ) <= 5 )
            {
                // Create new session without destroying the old one
                session_regenerate_id( false );

                // Grab current session ID and close both sessions to allow other scripts to use them
                $new_session = session_id();
                session_write_close();

                // Set session ID to the new one, and start it back up again
                session_id( $new_session );
                session_start();
            }
        }
    }
}

/* End of file Sessions.php */
/* Location: /application/core/Sessions.php */