<?php
/**
 * Router.php
 *
 * @version 1.0
 * @date 3/31/15 11:39 PM
 * @package rapidCore
 */

class Router {

	public static $routes = array();

	public static function set_group( $params = array() )
	{
		foreach( $params as $key => $params )
		{
			self::set( $key, $params );
		}
	}

	public static function set( $controller, $parameters = array() )
	{
		self::$routes[$controller] = (array)$parameters;
	}

	public static function get()
	{
		return self::$routes;
	}
}
//end Router