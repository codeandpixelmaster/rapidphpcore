<?php
/**
 * Helper.php
 * Series of static helper functions
 *
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @updated 25-Dec-2016
 * @package RapidPHPMe Core
 **/

class Helper {

	/**
	 * @param $address
	 * @return array
	 */
    public static function get_coordinates( $address )
    {
    	$return = array();

		//Unfortunately, the @ suppressor is needed to use the false !== response
    	$response = @file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true' );
		if( false === $response )
		{
			return false;
		}

    	$response = json_decode( $response );

		if( strtoupper( $response->status ) != 'OK' )
		{
			return false;
		}

    	$return['latitude'] = $response->results[0]->geometry->location->lat;
    	$return['longitude'] = $response->results[0]->geometry->location->lng;

    	return $return;
    }
    

}
//end Helper