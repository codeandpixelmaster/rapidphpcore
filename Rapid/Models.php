<?php
/**
 * Models.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

class Models {

	/**
	 * @type array
	 */
	private static $views = array();

	/**
	 * @type array
	 */
	private static $autoload = array();

	/**
	 * @type array
	 */
	private static $controllers = array();

	/**
	 * @type bool|SimplePDO
	 */
	public $db = false;

	/**
	 * @type null|Sessions
	 */
	private $session = null;

	/**
	 * @type array
	 */
	private $querystring;

	/**
	 * @type null
	 */
	private $class_segment = null;

	/**
	 * @type null
	 */
	private $requested_page = null;

	/**
	 * @type null
	 */
	public static $req_page = null;

	/**
	 * @type null
	 */
	private static $inst = null;

	public function __construct()
	{
		global $db;
		self::$views = Options::get_views();
		self::$autoload = Options::get_autoload();
		self::$controllers = Options::get_controllers();
		$this->db = $db;
		$parser = new UriParser();
		$this->querystring = $parser->get_params();
		$this->class_segment = $this->querystring['class_segment'];
		$this->requested_page = Models::filter_page( $this->querystring['page'] );
		self::$req_page = $this->requested_page;
		$this->session = Sessions::init();
		$this->load_request();
	}
	//end __construct()


	public static function init()
	{
		if( self::$inst == null )
		{
			self::$inst = new self();
		}
		return self::$inst;
	}
	//end init()


	private function load_request()
	{
		$checks = array(

			//Check the class and function that match
			array(
				$this->class_segment['class'] => $this->class_segment['function']
			),

			//Check if we can match the path and classmatched function from routers
			array(
				$this->querystring['firstpath'] => $this->class_segment['function']
			), 
			
			//Check if we can match the path and page from routers
			array(
				$this->querystring['firstpath'] => $this->querystring['page']
			),

			//Check to see if class and "controller_[function]" match
			array(
				$this->class_segment['class'] => 'controller_'.$this->class_segment['function']
			),

			//Check to see if class and "controller_[class]_[function]" match
			array(
				$this->class_segment['class'] => 'controller_' . strtolower( $this->class_segment['class'] ) . '_' . $this->class_segment['function']
			),

			//Check to see if we can match the class and a function called "index"
			array(
				$this->class_segment['class'] => 'index'
			),

			//Check if we can match the path and classmatched function from routers
			array(
				$this->querystring['firstpath'] => 'index'
			),

		);


		$found = false;
		foreach( $checks as $check )
		{
			foreach( $check as $class => $function )
			{

				if(
					in_array( $class .'.php', array_keys( self::$controllers ) ) &&
					method_exists( $class, $function ) &&
					is_callable( array(
						$class,
						$function,
					) ) &&
					!$found
				)
				{
					$found = true;

					$init = new $class;
					$params = $this->check_parameters( $function, $init );
					call_user_func_array( array( $init, $function ), $params );
					break;
				}
			}
		}
		if( !$found )
		{
			//SECURITY CHECK!!!
			if( defined( 'FORCE_CONTROLLER' ) && FORCE_CONTROLLER && !function_exists( 'controller_'.$this->requested_page ) )
			{
				self::view_file( '404' );
			}
			elseif( function_exists( 'controller_'.$this->requested_page ) )
			{
				$params = $this->check_parameters( 'controller_'. $this->requested_page );
				call_user_func_array( 'controller_'.$this->requested_page, $params );
			}
			else
			{
				if( array_key_exists( $this->requested_page, self::$views ) && file_exists( self::$views[$this->requested_page] ) )
				{
					include( self::$views[$this->requested_page] );
				}
				else
				{
					self::view_file( '404' );
				}
			}
		}

	}
	//end load_request()


	/**
	 * Loads files to view
	 *
	 * @access public
	 * @param string $file
	 * @param array $data
	 * @param bool $cache
	 * @param bool $flush (set to true to reset the cache, just remember to change it!)
	 * @return view
	 */
	public static function view_file( $file, $data = null, $cache = false, $flush = false )
	{
		Timer::start( 'Loading View: '. $file );

		global $db;

		$file = trim( $file );

		//Include the global viewlist and make the $db accessible to views with no excess

		if( array_key_exists( $file, self::$views ) && file_exists( self::$views[$file] ) )
		{
			if( $cache === true && in_array( 'cache', self::$autoload ) )
			{
				Models::cache_partial( self::$views[$file], self::$req_page, $data, $flush );
			}
			else
			{
				//Only extract the data here if we aren't caching this file
				if( is_array( $data ) )
				{
					extract( $data );
				}
				include_once( self::$views[$file] );
			}
		}
		else
		{
			echo '<h1>This is not the page you were looking for</h1>';
		}
		Timer::start( 'Finished loading View: '. $file );
	}
	//end view


	/**
	 * Handle internal caching of view file contents if desired from "view"
	 * Dependent on phpfastcache plugin being initialized in /plugins/
	 * @access private
	 * @param string $view file location of file to be cached
	 * @param string $requested == PAGE param to allow global elements to be cached with detailed data
	 * @param array $data optional
	 * @param bool $flush to enable in-function cache refreshing
	 * @return mixed
	 */
	private static function cache_partial( $view, $requested, $data = null, $flush = false )
	{

		global $cache;
		if( !in_array( 'cache', self::$autoload ) || !$cache )
		{
			include_once( $view );
			return;
		}

		//Make a bit of a hash from the requested URI and the current view
		$cache_file = md5( trim( $view . Models::filter_page( $requested ) ) );

		//Allow the file to be flushed with a one-line change
		if( $flush === true )
		{
			$cache->delete( $cache_file );
		}

		$file = $cache->get( $cache_file );
		if( $file == null )
		{
			ob_start();

			if( is_array( $data ) )
			{
				extract( $data );
			}

			include( $view );

			$file = ob_get_contents();

			//Check the config to see if we should auto append a cached timestamp for debugging
			$cache_config = Options::get_cache();
			if( $cache_config['debug'] === true )
			{
				$file .= '<!--cached timestamp: '. right_now().'-->';
			}

			$cache->set( $cache_file, $file, $cache_config['expiry'] );

			ob_end_clean();
		}

		echo $file;
	}
	//end cache_partial()


	/**
	 * Function to perform replacements on the global PAGE request
	 * generated by class.querystring.php
	 * @access public
	 * @param string $page
	 * @return string
	 */
	private static function filter_page( $page )
	{
		return str_replace( array( '-' ), array( '_' ), $page );
	}
	//end filter_page()


	/**
	 * Function to allow the checking of default params
	 * passed to controller_[whatever] functions to allow
	 * direct/full caching to be assigned within the controller_[whatever]
	 * function itself
	 *
	 * @param $controller
	 * @param null $class
	 * @return array
	 */
	private function check_parameters( $controller, $class = null )
	{
		$function_paramters = array();

		try {

			if( !is_null( $class ) )
			{
				$function = new ReflectionMethod( $class, $controller );
			}
			else
			{
				$function = new ReflectionFunction( $controller );
			}
			$parameters = $function->getParameters();
			if( count( $parameters ) > 0 )
			{
				foreach( $parameters as $param )
				{
					$function_paramters[$param->getName()] = $param->getDefaultValue();
				}
			}
		} catch( Exception $e ) {
			//do nothing...
		}

		return $function_paramters;
	}
	//end check_parameters()

}
//end class Models