<?php
/**
 * ConfigLoader.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

class ConfigLoader {
    
    public function __construct()
    {
        define( 'RAPID_CORE', dirname( __FILE__ ) . SEP );
        
        define( 'RAPID_HELPERS', dirname( __FILE__ ) . SEP . 'helpers'. SEP );
        
        define( 'RAPID_FUNCTIONS', dirname( __FILE__ ) . SEP . 'functions'. SEP );

        //Application directory
        define( 'APP', ROOT . SEP . 'application' . SEP );

        //Application core helpers and functions
        define( 'CORE', APP . 'core' . SEP );

        //Config directory
        define( 'CONFIG', APP . 'config' . SEP );

        //Logfile location, will be automatically created if it doesn't exist
        define( 'LOGS', APP . 'logs' );

        //Models
        define( 'MODELS', APP . 'models' . SEP );

        //Controllers
        define( 'CONTROLLERS', APP . 'controllers' . SEP );

        //Helpers directory, subdir of classes
        define( 'HELPERS', RAPID_CORE . 'helpers' . SEP );

        //Define user defined helpers
        define( 'USER_HELPERS', APP . 'helpers' . SEP );

        //Define user defined models
        define( 'USER_MODELS', APP . 'models' . SEP );

        //Plugins directory
        define( 'PLUGINS', APP . 'plugins' . SEP );

        //Layout directory.  Directory may be renamed
        define( 'VIEWS', APP . 'views' . SEP );

        //Layouts directory for constant elements
        define( 'LAYOUTS', VIEWS . 'layouts' . SEP );

        //Force controller usage or not.  Sets whether or not view() allows views to be loaded directly
        //This SHOULD be set to true, so only views that have a controller_page_name can be loaded
        define( 'FORCE_CONTROLLER', true );
        
        //Assign the constants
        $constants = Options::get_constants();

        //Auto retrieve the config
        $config = Options::get_config();

        //Assign the database params
        $db_options = Options::get_database();

        /**
         * Make sure the logs directory exists since it
         * isn't included in the package
         */
        if( !is_dir( LOGS ) )
        {
            mkdir( LOGS );
        }
        
        //Run logs to file from log_message function?
        define( 'RUN_LOGS', ( $config['log_errors'] !== false ) ? LOGS : false );
        
        //Max size of logfile if used
        define( 'LOG_SIZE', !empty( $config['log_size'] ) ? $config['log_size'] : 1 );
        

        /**
         * Allow usage of local logging wasp vs. wasp webservice
         */
        if( false === $config['wasp_full'] && class_exists( 'Whoops\Run' ) )
        {
            $whoops = new \Whoops\Run;
            $handler = new \Whoops\Handler\PrettyPageHandler;
            $handler->setEditor('textmate');
            $handler->setPageTitle(null);
            $whoops->pushHandler($handler);
            //If we're requesting via ajax, handle debugging to console.log
            if(
                ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' )
                || isset( $_SERVER['HTTP_X_PJAX'] )
            )
            {
                $ajax = new Whoops\Handler\JsonResponseHandler(); //->addTraceToOutput()
                $ajax->addTraceToOutput( true );
                $whoops->pushHandler($ajax);
            }
            $whoops->register();
        }
        else
        {
            $wasp_options = array( 
                'environment' => ENVIRONMENT, 
                'display' => $config['debug'], 
                'open' => '<pre>', 
                'close' => '</pre>', 
                'code' => true, 
                'ignore' => array(),  //Numeric error levels to ignore, 
                'ignored_domains' => array(),  //list of domains where errors should not go to wasp, 
                'generate_log' => RUN_LOGS, 
                'full_backtrace' => true
            );
            global $wasp;
            $wasp = new Wasp( $config['wasp_api_key'], $wasp_options );
        }

        //Assign the timezone
        date_default_timezone_set( $config['timezone'] );


        //Time constants
        define( 'MINUTE_IN_SECONDS', 60 );
        define( 'HOUR_IN_SECONDS', ( 60 * MINUTE_IN_SECONDS ) );
        define( 'DAY_IN_SECONDS', ( 24 * HOUR_IN_SECONDS ) );
        define( 'WEEK_IN_SECONDS', ( 7 * DAY_IN_SECONDS ) );
        define( 'MONTH_IN_SECONDS', ( 30 * DAY_IN_SECONDS ) );
        define( 'YEAR_IN_SECONDS', ( 365 * DAY_IN_SECONDS ) );

        //Sitename
        define( 'SITENAME', $config['sitename'] );

        //Current mode
        define( 'MODE', ENVIRONMENT );

        //Subdirectory  use? (bool)
        define( 'SUBDIR', $config['subdir'] );

        //Define directory name
        define( 'DIRNAME', $config['dirname'] );

        //Default page title separator
        define( 'PAGE_SEP', $config['title_separator'] );

        //Define the http path to the site for BASE
        define( 'SITE_URL', $config['uri'] );

        //Define BASE for linkages
        define( 'BASE', SITE_URL . DIRNAME );

        //Assets (js, css, images)
        define( 'ASSETS', BASE . SEP . 'assets' . SEP );

        //Absolute path to assets
        define( 'ASSETS_ROOT', ROOT . SEP . 'assets' . SEP );

        //Temporary files path
        define( 'TEMP_PATH', ASSETS_ROOT . 'files' );

        //Force SSL?
        define( 'FORCE_SSL', false );

        //Define the default CRSF input name used for form input validation
        define( 'CRSF_INPUT_NAME', $config['crsf_name'] );

        //Be sure to generate unique keys, good finds at: https://api.wordpress.org/secret-key/1.1/salt/
        define( 'E_ST', $config['encryption_key'] );

        //Start the database
        global $db;
        $db = false;

        //Database table prefix, optional but useful
        define( 'PREFIX', isset( $db_options['prefix'] ) ? $db_options['prefix'] : '' );

        if( !empty( $db_options['host'] ) && !empty( $db_options['username'] ) && !empty( $db_options['password'] ) )
        {
            //Definition params for class DB    
            $params = array(
                'host' => $db_options['host'], 
                'user' => $db_options['username'], 
                'password' => $db_options['password'], 
                'database' => $db_options['database']
            );

            try {
                $db = SimplePDO::getInstance( $params );   
            } catch( PDOException $e ) {
                trigger_error( $e->getMessage() );
            }
        }

        //Define any database tables as needed
        if( isset( $db_options['tables'] ) && is_array( $db_options['tables'] ) )
        {
            foreach( $db_options['tables'] as $def => $table )
            {
                if( !empty( $def ) && !empty( $table ) && !defined( strtoupper( $def ) ) )
                {
                    define( strtoupper( $def ), $db_options['prefix'] . $table );
                }
            }
        }

        //Define any other constants defined by the user
        if( !empty( $constants ) )
        {
            foreach( $constants as $def => $value )
            {
                if( !empty( $def ) )
                {
                    $def = strtoupper( $def );
                    if( !defined( $def ) )
                    {
                        define( strtoupper( $def ), $value );
                    }
                }
            }
        }
        
    }
    //end construct
    
}
//end ConfigLoader