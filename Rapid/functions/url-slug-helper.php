<?php
/**
 * url-slug-helper.php
 * application/helpers/url-slug-helper.php
 * Functions to generate distinct URL permalink slugs for dynamic content
 *
 * @version 1.0
 * @date 19-Mar-2014
 * @package RapidPHPMe
 * @dependencies class SimplePDO()
 *
 * Table of contents:
 *
 ** _truncate_slug()        //truncate overall length of a slug to 200 chars or less
 ** unique_slug()           //compares provided slug and id against others to generate unique
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

/**
 * Truncate a string used for a permalink
 *
 * @access private
 * @param string $slug
 * @param int $length
 * @return string
 */
function _truncate_slug( $slug, $length = 200 )
{
    if( strlen( $slug ) > $length )
    {
        $decoded_slug = urldecode( $slug );
        if( $decoded_slug === $slug )
        {
            $slug = substr( $slug, 0, $length );   
        }
        else
        {
            $slug = $slug;
        }
    }
    return rtrim( $slug, '-' );
}


/**
 * Function to query the DB against other possible conflicts with slugs
 *
 * Example usage:
 * $data = array(
 *    'table' => 'YOURTABLENAME', 
 *    'column' => 'COLUMN TO CHECK SLUG FOR', 
 *    'id_column' => 'entry_id', 
 *    'entry_id' => $id
 * );
 * echo unique_slug( $slug, $data );
 *
 * @access public
 * @param string $slug
 * @param array $data( 'table' => REQUIRED, 'column' => REQUIRED, 'id_column' => '', 'entry_id' => '' )
 * @return string
 */
function unique_slug( $slug, $data = array() )
{
    global $db;
    
    $original_slug = $slug;
    
    //If there isn't a db table or column to cross check, then just return the original value
    if( !isset( $data['table'] ) || !isset( $data['column'] ) || !$db )
    {
        return $slug;
    }

    //Slugs must be unique across all posts.
    $params = array();
    
    $check_sql = "SELECT COUNT(".$data['column'].") FROM ".$data['table']." WHERE ".$data['column']." = ?";
    $params[] = $slug;
    
    if( isset( $data['entry_id'] ) && isset( $data['id_column'] ) )
    {
        $check_sql .= " AND ".$data['id_column']." != ?";
        $params[] = $data['entry_id'];
    }
    $check_sql .= " LIMIT 1";
    $post_name_check = $db->num_rows( $check_sql, $params );

    if( $post_name_check > 0 )
    {
        $ext = 2;
        do {
            $alt_post_name = _truncate_slug( $slug, 200 - ( strlen( $ext ) + 1 ) ) . "-$ext";
            
            $params = array();
            
            $check_sql = "SELECT COUNT(".$data['column'].") FROM ".$data['table']." WHERE ".$data['column']." = ?";
            $params[] = $alt_post_name;
            if( isset( $data['entry_id'] ) && isset( $data['id_column'] ) )
            {
                $check_sql .= " AND ".$data['id_column']." != ?";
                $params[] = $data['entry_id'];
            }
            $check_sql .= " LIMIT 1";
            $post_name_check = $db->num_rows( $check_sql, $params );
            $ext++;
        } while ( $post_name_check > 0 );
        
        $slug = $alt_post_name;
    }
    return $slug;
}


/* End of file url-slug-helper.php */
/* Location: application/helpers/url-slug-helper.php */