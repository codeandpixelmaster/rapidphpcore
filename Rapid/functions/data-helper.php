<?php
/**
 * data-helper.php
 * application/helpers/data-helper.php
 * Assorted data cleaning, formatting, and syntax helpers
 *
 * @version 1.0
 * @date <?php
/**
 * data-helper.php
 * application/helpers/data-helper.php
 * Assorted data cleaning, formatting, and syntax helpers
 *
 * @version 1.1
 * @date 26 Aug 2015
 * @updated 30-Jun-2016
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** is_serial                    //Check if a string is serialized
 *  include_get_contents()  	//Get and execute PHP contents from other files
 * env()						//Get and set defaults for environmental variables
 *
 **/

if( !function_exists( 'is_serial' ) )
{
	/**
	 * Check if a string is serialized
	 * @param string $string
	 * @return bool
	 */
	function is_serial( $string )
	{
		return ( @unserialize($string) !== false || $string == 'b:0;' );
	}
}


if( !function_exists( 'include_get_contents' ) )
{
	/**
	 * include_get_contents
	 * include a file and return it's output
	 *
	 * @param string $path filename of include
	 * @return string
	 */
	function include_get_contents( $path )
	{
		if( file_exists( $path ) )
		{
			ob_start();
			include_once( $path );
			return ob_get_clean();
		}
	}
}

if( !function_exists( 'env' ) )
{
	/**
	 * Gets the value of an environment variable. Supports boolean, empty and null.
	 *
	 * @param  string  $key
	 * @param  mixed   $default
	 * @return mixed
	 */
	function env( $key, $default = null )
	{
		$value = getenv( $key );

		if( $value === false )
		{
			return $default;
		}

		switch( strtolower( $value ) )
		{
		case 'true':
		case '(true)':
			return true;
		case 'false':
		case '(false)':
			return false;
		case 'empty':
		case '(empty)':
			return '';
		case 'null':
		case '(null)':
			return null;
		}

		if( strlen( $value ) > 1 && startsWith( $value, '"' ) === '"' && endsWith($value, '"') )
		{
			return substr( $value, 1, -1 );
		}

		return $value;
	}
}


if( !function_exists( 'startsWith' ) )
{
	/**
	 * @src http://stackoverflow.com/a/10473026
	 * @param $haystack
	 * @param $needle
	 * @return bool
	 */
	function startsWith( $haystack, $needle )
	{
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}
}


if( !function_exists( 'endsWith' ) )
{
	/**
	 * @src http://stackoverflow.com/a/10473026
	 * @param $haystack
	 * @param $needle
	 * @return bool
	 */
	function endsWith( $haystack, $needle )
	{
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}
}

/* End of file data-helper.php */
/* Location: application/helpers/data-helper.php */