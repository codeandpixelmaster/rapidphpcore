<?php
/**
 * form-helper.php
 * Provides support for CRSF, automated form generation
 *
 * @version 1.0
 * @date 19-Sep-2013
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** crsf()          //generate CRSF token and assign to hidden input
 ** check_crsf()    //check CRSF token generated using crsf()
 ** form()          //generate a form
 **/

 /**
  * Function to generate random key and assign it to a session variable to confirm authenticity
  * Usage:
  *
  * <form action="/file/" method="post">
  *     <?php crsf('data_token'); ?> //Outputs hidden input with session variable set
  *     <input type="text" name="other_values" value="" />
  *     <button type="submit" name="Submit" id="Submit" >Submit</button>
  * </form>
  *
  * @access public
  * @param string input name
  * @return string hidden input
  */
if( !function_exists( 'crsf' ) )
{

    function crsf( $echo = true, $name = CRSF_INPUT_NAME )
    {
        new Sessions();
        
        $token= md5( uniqid() );
        
        //Accomodate multiple auth tokens utilized by multiple forms on same page
        $_SESSION['crsf'][$name] = $token;
        
        $output = '<input type="hidden" name="'.$name.'" value="'.$token.'" />';
        if( $echo )
        {
            echo $output;
        }
        else
        {
            return $output;
        }
    }

}


/**
 * Function to check for random key and matched session variable to confirm authenticity
 * Usage:
 *
 * if( isset($_POST['submit']) && check_crsf('data_token') )
 * {
 *      //process form data
 * }
 *
 * @access public
 * @param string post/get input name
 * @return bool
 */
function check_crsf( $comparison, $name = '' )
{
    new Sessions();
    if( empty( $comparison ) || !isset( $_SESSION['crsf'][$name] ) || empty( $_SESSION['crsf'][$name] ) )
    {
        return false;
    }
    else
    {
        $checksum = $_SESSION['crsf'][$name];
        unset( $_SESSION['crsf'][$name] );
        
        if( !empty( $comparison ) )
        {
            if( $comparison == $checksum )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}


/**
 * Function to generate forms dynamically with PHP arrays
 * Example usage:
 *
 * $fields = array(
 * 	array( 'label' => 'Your Email', 'type' => 'email', 'name' => 'email_address', 'class' => 'required' ), 
 * 	array( 'label' => 'Your Name', 'type' => 'text', 'name' => 'user_name', 'class' => 'required' ), 
 * 	array( 'label' => 'Why I should care?', 'type' => 'textarea', 'name' => 'no-care' )
 * );
 * form( $fields );
 *
 * @access public
 * @param array $fields
 * @param bool $crsf
 * @param string $action
 * @param string $form_class (or id)
 * @param string $submit_text
 * @return string
 */
if( !function_exists( 'form' ) )
{
    
    function form( $fields = array(), $crsf = false, $action = '', $form_class = '', $submit_text = 'Submit' )
    {

        $form = '<form action="'.$action.'" method="post"'.$form_class.'>' . PHP_EOL;

        //Include CRSF input
        if( $crsf )
        {
            $form .= crsf( false, CRSF_INPUT_NAME ) . PHP_EOL;
        }

        foreach( $fields as $field )
        {
    		//Handle adding field labels
    		if( isset( $field['label'] ) && !empty( $field['label'] ) )
    		{
    			$form .= '<label for="'.$field['name'].'">'.$field['label'].'</label>' . PHP_EOL;
    		}

    		//Handle added classes
    		$class = '';
    		if( isset( $field['class'] ) && !empty( $field['class'] ) )
    		{
    			$class = ' class="'.$field['class'].'"';
    		}

    		//Handle prepopulated input values
    		$value = '';
    		if( isset( $field['value'] ) && !empty( $field['value'] ) )
    		{
    			$value = $field['value'];
    		}

    		//Text, email, password
    		if( $field['type'] == 'text' || $field['type'] == 'email' || $field['type'] == 'password' )
    		{
    			$form .= '<input type="'.$field['type'].'" name="'.$field['name'].'" value="'.$value.'"'.$class.' />' . PHP_EOL;
    		}
    		//Textarea
    		if( $field['type'] == 'textarea' )
    		{
    			$form .= '<textarea name="'.$field['name'].'" rows="" cols=""'.$class.'>'.$value.'</textarea>' . PHP_EOL;
    		}
    		//Hidden
    		if( $field['type'] == 'hidden' )
    		{
    			$form .= '<input type="'.$field['type'].'" name="'.$field['name'].'" value="'.$value.'"'.$class.' />' . PHP_EOL;
    		}

        }

        $form .= '<input type="submit" value="'.$submit_text.'" name="submit" />' . PHP_EOL;

        $form .= '</form>' . PHP_EOL;

        echo $form;
    }
       
}

/* End of file form-helper.php */
/* Location: application/helpers/form-helper.php */