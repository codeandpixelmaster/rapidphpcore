<?php
/**
 * Paginate.php
 * Usage:
 *

$page_number = isset( $_GET['page'] ) ? $_GET['page'] : 1;
$query = "SELECT * FROM yourtable WHERE id = ? AND level = ?";
$params = array( 1, 5 );

$pager = new Paginate();
$pager->setCurrentPage( $page_number );
$pager->setWrapper( '<div id="yourdivid" class="yourclass'>', '</div>' );
$page->setClass( 'active-nav' );
$pager->setSql( $query );
$results = $pager->execute( $params );
$pagination = $pager->getUIPagination();

 *
 *
 * @version 1.0
 * @date 8/31/15 11:16 PM
 * @updated 12-Oct-2016
 * @package RapidPHP
 */

class Paginate extends Control {

	public $current_page = 1;
	public $records_per_page = 25;
	public $total_records = 0;
	public $total_pages = 0;
	public $results;
	public $from;
	public $to;
	public $params;
	public $start = '<div class="pagination">';
	public $end = '</div>';
	public $active_class = 'active';
	public $disabled_class = 'disabled';
	public $container_class = 'pagination-counter';


	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Used for the <div containing the pagination
	 * @param string $start
	 * @param string $end
	 */
	public function setWrapper( $start = '', $end = '' )
	{
		$this->start = !empty( $start ) ? $start : $this->start;
		$this->end = !empty( $end ) ? $end : $this->end;
	}

	/**
	 * Used for the active button class
	 * @param string $class
	 */
	public function setClass( $class )
	{
		$this->active_class = $class;
	}

	/**
	 * Used for the <ul class="here"> wrapper for pagination buttons
	 * @param string $class
	 */
	public function setContainerClass( $class )
	{
		$this->container_class = $class;
	}


	/**
	 * Used to control the <li> class for disabled elipses
	 * @param string $class
	 */
	public function setDisabledClass( $class )
	{
		$this->disabled_class = $class;
	}

	/**
	 * @param int $value
	 */
	public function setCurrentPage( $value )
	{
		$this->current_page = $value;
	}

	/**
	 * @param int $value
	 */
	public function setRecordsPerPage( $value = '' )
	{
		$this->records_per_page = !empty( $value ) ? $value : $this->records_per_page;
	}

	/**
	 * @return bool
	 */
	public function hasPager()
	{
		return $this->getTotalPages() > 1;
	}

	/**
	 * @return bool
	 */
	public function showPrevious()
	{
		return ( $this->getTotalPages() > 1 && $this->current_page > 1 );
	}

	/**
	 * @return bool
	 */
	public function showNext()
	{
		return ( $this->getTotalPages() > 1 && $this->current_page < $this->getTotalPages() );
	}

	/**
	 * @return int
	 */
	public function getPreviousPageNumber()
	{
		return ( $this->current_page > 1 ) ? $this->current_page - 1 : 1;
	}

	/**
	 * @return float|int
	 */
	public function getNextPageNumber()
	{
		return ( $this->current_page < $this->getTotalPages() ) ? $this->current_page + 1 : $this->getTotalPages();
	}

	/**
	 * @param string $sql
	 * @return string
	 */
	public function setSql( $sql )
	{
		$this->sql = $sql;
		return $this->sql;
	}

	/**
	 * @param array $params
	 */
	public function setParams( $params )
	{
		$this->params = $params;
	}

	/**
	 * @return mixed
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * @return mixed
	 */
	public function getSql()
	{
		return $this->sql;
	}

	/**
	 * @return mixed
	 */
	public function getResultSet()
	{
		return $this->results;
	}

	/**
	 * @return string
	 */
	private function getPagerSql()
	{
		$sql = preg_replace( '@select@i', 'SELECT SQL_CALC_FOUND_ROWS ', $this->getSql(), 1 );

		if( $this->current_page == 1 )
		{
			$from = '0';
		}
		else
		{
			$from = ( $this->current_page - 1 ) * $this->records_per_page;
		}
		$sql.= " LIMIT {$from}, {$this->records_per_page}";
		return $sql;
	}

	/**
	 * @param array $params
	 * @return mixed
	 */
	public function execute( $params = null )
	{
		if( $params != null )
		{
			$this->params = $params;
		}
		$this->results = $this->db->get_results( $this->getPagerSql(), $this->params );

		$rpRes = $this->db->get_row( 'SELECT FOUND_ROWS() as total' );
		$this->total_records = $rpRes->total;
		$this->to = ( $this->current_page == 1 ) ? $this->records_per_page : $this->records_per_page * $this->current_page;

		$this->from = max( ( $this->records_per_page * ( $this->current_page - 1 ) ) + 1, 1 );
		$this->to = min( $this->total_records, $this->records_per_page * $this->current_page );
		$this->getTotalPages();
		return $this->results;
	}

	/**
	 * @return int
	 */
	public function getTotalPages()
	{
		if( !( $this->total_records % $this->records_per_page ) )
		{
			$this->total_pages = ( $this->total_records / $this->records_per_page );
		}
		else
		{
			$this->total_pages = floor( $this->total_records / $this->records_per_page ) + 1;
		}
		return $this->total_pages;
	}


	/**
	 * @return int
	 */
	public function getTotalRecords()
	{
		return $this->total_records;
	}


	/**
	 * @param bool|false $select Used to output pagination as select menu
	 * @param int|null $links_to_show Used to show or hide dot
	 * @return bool|string
	 */
	public function getUIPagination( $select = false, $links_to_show = null )
	{
		$params = array_filter( $_GET, function ( $v ) {
			return !is_null( $v ) && $v !== '';
		});
		if( isset( $params['page'] ) )
		{
			unset( $params['page'] );
		}

		$uri = explode( "?", $_SERVER['REQUEST_URI'] );
		$url = ( count( $params ) ) ? $uri[0] . "?" . http_build_query( $params ) . "&" : $uri[0].'?';

		if( $this->hasPager() )
		{
			if( $select )
			{
				$pages = array();
				foreach( range( 1, $this->getTotalPages() ) as $i )
				{
					$active = ( $this->current_page == $i ) ? ' selected' : '';
					$pages[] = "<option value='{$url}page={$i}'{$active}>{$i}</option>";
				}

				$menu = '<select class="" onchange="if (this.value) window.location.href=this.value">';
				$menu .= '<option value="">Jump to Page...</option>';
				$menu .= implode( PHP_EOL, $pages );
				$menu .= '</select>';
				return $menu;
			}

			$max_show = ( !is_null( $links_to_show ) ) ? ( (int)$links_to_show ) : $this->getTotalPages();
			$end = ( ( $this->current_page + $max_show ) < $this->getTotalPages() ) ? $this->current_page + $max_show : $this->total_pages;
			$start = ( ( $this->current_page - $max_show ) > 0 ) ? $this->current_page - $max_show : 1;
			$show_dots = false;
			if( !is_null( $links_to_show ) && $this->getTotalPages() > $links_to_show )
			{
				$show_dots = true;
			}

			$pages = [];
			if( $this->current_page > 1 )
			{
				$link = ( $this->current_page > 2 || $this->current_page < ( $this->getTotalPages() - 1 ) ) ? ( $this->current_page - 1 ) : 1;
				$pages[]= "<li><a href='{$url}page={$link}'>Prev</a></li>";
				if( $show_dots )
				{
					$pages[] = '<li class="'.$this->disabled_class.'"><span>...</span></li>';
				}
			}


			for( $i = $start; $i <= $end; $i++ )
			{
				$active = ( $this->current_page == $i ) ? 'class="'.$this->active_class.'"' : '';
				$pages[] = "<li {$active}><a href='{$url}page={$i}'>{$i}</a></li>";
			}

			if( $this->current_page < $this->total_pages )
			{
				$link = ( $this->current_page > 2 || $this->current_page < ( $this->total_pages - 1 ) ) ? ( $this->current_page + 1 ) : $this->total_pages;

				if( $show_dots )
				{
					$pages[] = '<li class="'.$this->disabled_class.'"><span>...</span></li>';
				}
				$pages[] = "<li><a href='{$url}page={$link}'>Next</a></li>";
			}

			return $this->start . "<ul class=\"".$this->container_class."\">" . implode( '', $pages ) . "</ul>". $this->end;
		}
		return false;

	}
}
//end Paginate