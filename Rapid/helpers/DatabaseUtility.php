<?php
/**
 * DatabaseUtility.php
 * Utility designed to consolidate basic database operations
 *
 * Seed usage:
 *
 * $seed = new DatabaseUtility();
 * $seed->add( 'new_table' )
 * ->column( 'user_id', 'int(11)', 'DEFAULT NULL', true, true )
 * ->column( 'user_name', 'varchar(220)', 'DEFAULT NULL', false, true )
 * ->column( 'user_level', 'tinyint(1)', 'DEFAULT "9"', false )
 * ->run();
 *
 * @version 1.0
 * @date 8/28/15 5:26 AM
 * @package rapidCore
 */

class DatabaseUtility extends Control {

	private $query;
	private $primary_key;
	private $cols = array();
	private $keys = array();
	private $engine = 'MyISAM';
	private $charset = 'utf8';

	/**
	 * @type array
	 */
	private $coltypes = [
		'varchar' => 'string',
		'char' => 'string',
		'text' => 'string',
		'longtext' => 'string',
		'enum' => 'string',
		'date' => 'dateTime',
		'timestamp' => 'dateTime',
		'int' => 'int',
		'tinyint' => 'int',
		'bigint' => 'int',
		'float' => 'float',
		'double' => 'float',
		'bool' => 'bool',
		'boolean' => 'bool',
	];


	/**
	 * @param null $engine
	 * @param null $charset
	 * @throws Exception
	 */
	public function __construct( $engine = null, $charset = null )
	{
		parent::__construct();
		if( !$this->db )
		{
			throw new Exception( 'No database initialized.' );
		}
		if( !is_null( $engine ) )
		{
			$this->engine = $engine;
		}
		if( !is_null( $charset ) )
		{
			$this->charset = $charset;
		}
	}


	/**
	 * @param $table
	 * @return $this
	 */
	public function add( $table )
	{
		$this->query = 'CREATE TABLE IF NOT EXISTS `'. $table .'` (';
		return $this;
	}


	/**
	 * @param $name
	 * @param string $type
	 * @param null $default
	 * @param bool $primary
	 * @param bool $key
	 * @return $this
	 */
	public function column( $name, $type = 'varchar(220)', $default = null, $primary = false, $key = false )
	{
		//`user_failed_logins` tinyint(1) DEFAULT '0',
		$this_column = "`$name` $type";

		if( !is_null( $default ) )
		{
			$this_column .= " $default";
		}

		if( $primary === true )
		{
			$this_column .= " AUTO_INCREMENT";
			$this->primary_key = "PRIMARY KEY(`$name`)";
		}

		$this->cols[] = $this_column;

		if( $key !== false )
		{
			$this->keys[] = "KEY (`$name`)";
		}

		return $this;
	}


	/**
	 * @return bool
	 */
	public function run()
	{
		if( empty( $this->query ) || empty( $this->cols ) )
		{
			return false;
		}
		$this->query .= implode( ', '. PHP_EOL, $this->cols );
		if( !empty( $this->primary_key ) )
		{
			$this->query .= ', '.PHP_EOL . $this->primary_key .', ';
		}
		if( !empty( $this->keys ) )
		{
			$this->query .= PHP_EOL . implode( ', '. PHP_EOL, $this->keys );
		}
		$this->query = rtrim( trim( $this->query ), ',' );
		$this->query .= PHP_EOL . ') ENGINE='. $this->engine .' DEFAULT CHARSET='.$this->charset .';';
		return $this->db->query( $this->query );

	}


	/**
	 * Create a table if it doesn't already exist
	 *
	 * @param string $table
	 * @param string $create_sql
	 * @return bool
	 */
	public function create_table( $table, $create_sql )
	{
		//Table already exists, leave it
		if( $this->db->table_exists( $table ) )
		{
			return true;
		}

		$this->db->query( $create_sql, array() );
		if( $this->db->table_exists( $table ) )
		{
			return true;
		}

		return false;

	}
	//end create_table()


	/**
	 * Add a column to an existing table if the table exists
	 *
	 * @param string $table Database table name
	 * @param string $column Database Column name
	 * @param string $create_sql SQL to add column to table
	 * @return bool
	 */
	public function add_column( $table, $column, $create_sql )
	{
		if( !$this->db->table_exists( $table ) )
		{
			return false;
		}

		//Find out if this column already exists
		foreach( $this->db->list_fields( $table ) as $col )
		{
			if( $col == $column )
			{
				return true;
			}
		}

		//Add the column
		$this->db->query( $create_sql, array() );

		//Make sure the column exists
		foreach( $this->db->list_fields( $table ) as $col )
		{
			if( $col == $column )
			{
				return true;
			}
		}

		return false;

	}
	//end add_column()


	/**
	 * Drop a column from a database table if it exists
	 * @param string $table
	 * @param string $column
	 * @param string $drop_sql
	 * @return bool
	 */
	public function drop_column( $table, $column, $drop_sql )
	{
		if( !$this->db->table_exists( $table ) )
		{
			return false;
		}

		//Find out if this column already exists
		foreach( $this->db->list_fields( $table ) as $col )
		{
			if( $col == $column )
			{
				//We found it, so try to drop it
				$this->db->query( $drop_sql, array() );

				//Recheck directly to see if the column is still here
				foreach( $this->db->list_fields( $table ) as $col )
				{
					//Column still exists, query didn't work
					if( $col == $column )
					{
						return false;
					}
				} //end recheck to see if column is still there

			} //end col == column

		} //end foreach

		return true;

	}
	//end drop_column()


	/**
	 * Perform analysis on an existing table for SQL recommendations on performance
	 *
	 * @param $table
	 * @param int $rows
	 * @return object
	 */
	public function analyze( $table, $rows = 1000 )
	{
		if( !$this->db->table_exists( $table ) )
		{
			return false;
		}

		return $this->db->get_results( "SELECT * FROM $table PROCEDURE ANALYSE($rows, 50)", array() );

	}
	//end analyze()


	/**
	 * Check to see if a column in a db table is as we expected
	 * Usage:
	 * $helper = new DatabaseUtility();
	 * $correct = $helper->check_column( 'yourtablename', 'fieldnamehere', 'datetime' );
	 * $correct2 = $helper->check_column( 'yourtablename', 'fieldnamehere', 'varchar(200)' );
	 * if( !$correct || !$correct2 )
	 * 	//Do stuff
	 *
	 * @param string $table_name
	 * @param string $col_name
	 * @param string $col_type
	 * @param null $is_null
	 * @param null $key
	 * @param null $default
	 * @param null $extra
	 * @return bool
	 */
	public function check_column(
		$table_name,
		$col_name,
		$col_type,
		$is_null = null,
		$key = null,
		$default = null,
		$extra = null )
	{

		$diffs = 0;
		$results = $this->db->get_results( "DESC $table_name", array() );

		foreach ($results as $row )
		{
			if ( $row->Field == $col_name )
			{

				// Got our column, check the params.
				if( ( $col_type != null ) && ( $row->Type != $col_type ) )
				{
					++$diffs;
				}
				if( ( $is_null != null ) && ( $row->Null != $is_null ) )
				{
					++$diffs;
				}
				if( ( $key != null ) && ( $row->Key != $key ) )
				{
					++$diffs;
				}
				if( ( $default != null ) && ( $row->Default != $default ) )
				{
					++$diffs;
				}
				if( ( $extra != null ) && ( $row->Extra != $extra ) )
				{
					++$diffs;
				}

				//We found differences between the expected and existing
				if( $diffs > 0 )
				{
					return false;
				}

				return true;

			} // end if found our column

		} //end foreach

		return false;

	}
	//end check_column()


	/**
	 * Generate output to use for Class Variables based on database columns
	 * Ouputs something such as...
	 * /**
	 *  * @type int
	 *  * /
	 * var $fieldname; / var $fieldname = 'default';
	 *
	 *
	 * @param $table
	 * @param string $implode_as
	 * @return bool
	 * @throws Exception
	 */
	public function generate_variables( $table, $implode_as = '<br />' )
	{
		try {
			$query = $this->db->get_results( "DESCRIBE `". $table ."`" );
		} catch( Exception $e ) {
			throw new Exception( $e->getMessage() );
		}

		if( empty( $query ) )
		{
			return false;
		}
		$vars = [];
		foreach( $query as $value )
		{
			$type = preg_replace( '/[\[{\(].*[\]}\)]/U' , '', $value->Type );
			$type = isset( $this->coltypes[$type] ) ? $this->coltypes[$type] : $this->coltypes['varchar'];
			$string = '/**' . PHP_EOL;
			$string .= ' * @type '. $type . PHP_EOL;
			$string .= ' */' . PHP_EOL;

			$suffix = '';
			if( strlen( $value->Default ) > 0 )
			{
				$suffix =  ' = '. $value->Default;
				if( !ctype_digit( $value->Default ) )
				{
					$suffix = ' = \''. $value->Default .'\'';
				}

			}
			$varname = '$' . $value->Field . $suffix .';';
			$string .= 'public '. $varname . PHP_EOL;
			$output[] = $string;
		}

		return implode( $implode_as, $output );
	}


}
//End DatabaseUtility