<?php
/**
 * view-helper.php
 * Provides single-function initialization support for view() function
 * used in controller_[whatever] functions to load templates and views
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

/**
 * @param string $file
 * @param array $data
 * @param bool $cache
 * @param bool $flush
 */
function view( $file, $data = null, $cache = false, $flush = false )
{
    global $db, $controllers;
    $control = Models::view_file( $file, $data, $cache, $flush );
}

/**
 * Function to retrieve the current page request as loaded by Models
 * @return null|string
 */
function current_page()
{
    return !is_null( Models::$req_page ) ? Models::$req_page : null;
}