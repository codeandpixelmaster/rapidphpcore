<?php
/**
 * Debug.php
 * Src: https://github.com/panique/pdo-debug
 * Adapted to be part of core
 *
 * @version 1.0
 * @date 9/12/15 11:36 PM
 * @package rapidCore
 */

class Debug {

	/**
	 * debugPDO
	 *
	 * Shows the emulated SQL query in a PDO statement. What it does is just extremely simple, but powerful:
	 * It combines the raw query and the placeholders. For sure not really perfect (as PDO is more complex than just
	 * combining raw query and arguments), but it does the job.
	 *
	 * @author Panique
	 * @param string $raw_sql
	 * @param array $parameters
	 * @param bool $html (use htmlentities?)
	 * @return string
	 */
	static public function show( $raw_sql, $parameters, $html = false )
	{
		$keys = array();
		$values = $parameters;

		foreach ($parameters as $key => $value) {

			// check if named parameters (':param') or anonymous parameters ('?') are used
			if (is_string($key)) {
				$keys[] = '/:'.$key.'/';
			} else {
				$keys[] = '/[?]/';
			}

			// bring parameter into human-readable format
			if (is_string($value)) {
				$values[$key] = "'" . $value . "'";
			} elseif (is_array($value)) {
				$values[$key] = implode(',', $value);
			} elseif (is_null($value)) {
				$values[$key] = 'NULL';
			}
		}

		$raw_sql = preg_replace($keys, $values, $raw_sql, 1, $count);

		return ( $html ) ? htmlentities( $raw_sql ) : $raw_sql;
	}
}