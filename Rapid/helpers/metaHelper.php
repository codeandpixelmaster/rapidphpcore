<?php
/**
 * metaHelper.php
 * Classes to assist with common css, js, and meta outputs
 *
 * @version 1.0
 * @date 13-Oct-2012
 * @updated 01-Jun-2013
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** meta()           //outputs basic charset and property meta tags
 ** load_script()    //loads CSS and JS files using simplified arrays
 **/

class metaHelper {
    
    public $content, $name;
    
    
    /**
     * Function to output basic charset, keyword, the fb meta tags
     * @access public
     * @param string $name
     * @param string $content
     * @return string
     */
    public function meta( $name, $content )
    {
        if( !empty( $name ) && !empty( $content ) )
        {
            $this->name = trim( strtolower( $name ) );
            $this->content = htmlentities( trim( $content ) );
            
            if( $this->name == 'charset' )
            {
                echo '<meta charset="'.$this->content.'">' . PHP_EOL;
                //echo '<meta http-equiv="Content-Type" content="text/html; charset='.$this->content.'">' . PHP_EOL;   
            }
            elseif( $this->name == 'favicon' )
            {
                echo '<link rel="shortcut icon" type="image/x-icon" href="'.$this->content.'" />' . PHP_EOL;
            }
            elseif( stristr( $this->name, 'og' ) )
            {
                echo '<meta property="'.$this->name.'" content="'.$this->content.'" />' . PHP_EOL;
            }
            elseif( $this->name == 'fbapp' )
            {
                echo '<meta property="fb:app_id" content="'.$this->content.'" />' . PHP_EOL;
            }
            elseif( $this->name == 'html5' )
            {
                echo '<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->' . PHP_EOL;
            }
            else
            {
                echo '<meta name="'.$this->name.'" content="'.$this->content.'" />' . PHP_EOL;
            }
            
        }
    }
    

    /**
     * Function to load CSS and JS files using simplified arrays
     * Array passed to function must have specified key => value arrangement, where key is filename, and value is version
     * Usage:   
     * load_script( array( 'http://test.com/css/styles.css?ver=1.09', '/scripts.js') );
     *
     * Accepts true/false params to echo
     * $files = array('script1.js?v=1.09', 'script2.css', 'jquery', 'jqueryui');
     * load_script( $files [,false | true ] );
     * Autodetects script type, for example 'custom-scripts.inc.js.php' will output as js
     *
     * Will load assorted scripts from google libraries API by passing name param
     * https://developers.google.com/speed/libraries/devguide
     *
     * @access public
     * @param array $scripts
     * @param bool $use_headjs
     * @param bool $add_fallback
     * @param bool $echo
     * @return string
     */
    public function load_script( $scripts, $use_headjs = false, $add_fallback = false, $echo = true )
    {
        if( !empty( $scripts ) )
        {

            //Array of accepted param names and locations from Google libraries API
            $google_scripts = array();
            $google_scripts['jquery'] = '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js';
            $google_scripts['jqueryui'] = '//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.js';
            $google_scripts['mootools'] = '//ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js';
            $google_scripts['webfont'] = '//ajax.googleapis.com/ajax/libs/webfont/1.0.28/webfont.js';
            $google_scripts['prototype'] = '//ajax.googleapis.com/ajax/libs/prototype/1.7.1.0/prototype.js';
            $google_scripts['dojo'] = '//ajax.googleapis.com/ajax/libs/dojo/1.7.3/dojo/dojo.js';
            $google_scripts['chromeframe'] = '//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js';
            $google_scripts['googlemaps'] = '//maps.googleapis.com/maps/api/js?sensor=false';
            $google_scripts['googlecharts'] = '//www.google.com/jsapi';

            $script = array();
            $style = array();
            $js_fallback = '';
            $headjs = array();
            $headjs_starter = '';
            
            //Add jquery fallback if desired
            if( $add_fallback )
            {
                $js_fallback = '<script>if(typeof jQuery==\'undefined\'){document.write(\'<script type="text/javascript" src="'.ASSETS.'js/jquery-1.8.2.min.js" charset="utf-8"><\'+\'/script>\');}</script>' . PHP_EOL;
            }
            
            //Use head.js if desired
            if( $use_headjs )
            {
                $headjs_starter = '<script src="'.ASSETS.'/js/head.js"></script>';
            }

            if( is_array( $scripts ) )
            {
                foreach( $scripts as $script_name )
                {
                    $name = trim( $script_name );

                    // Check to see if we should be loading from Google
                    if( array_key_exists( $name, $google_scripts ) && !empty( $google_scripts[$name] ) )
                    {
                        $name = $google_scripts[$name];
                    }

                    //Determine the file type
                    if( preg_match('/.js/', $name) )
                    {
                        $type = 'js';
                    }
                    if( preg_match('/.css/', $name) )
                    {
                        $type = 'css';
                    }
                    
                    //Use head.js if desired
                    if( $use_headjs )
                    {
                        $headjs[] = "'" . $name . "'";
                    }

                    switch( $type )
                    {

                        case 'js':
                            $script[] = '<script src="' . $name . '"></script>';
                        break;

                        case 'css':
                            $style[] = '<link rel="stylesheet" href="' . $name . '" />';
                        break;

                        default:
                            $script[] = '<script src="' . $name . '"></script>';
                        break;

                    } // end switch
                }
            }

            if( $echo === false )
            {
                return $script;
            }
            else
            {
                echo implode( PHP_EOL , $style ) . PHP_EOL;
                
                if( !empty( $headjs_starter ) )
                {
                    echo $headjs_starter . PHP_EOL;
                    echo '<script>' . PHP_EOL;
                    echo 'head.js( '.implode(', ', $headjs).' );' . PHP_EOL;
                    echo '</script>' . PHP_EOL;
                    if( !empty( $js_fallback ) )
                    {
                        echo $js_fallback;
                    }
                }
                else
                {
                    echo implode( PHP_EOL , $script ) . PHP_EOL;  
                    if( !empty( $js_fallback ) )
                    {
                        echo $js_fallback;
                    } 
                }
            }
        } // end not empty $name
        
    } //End function
    
} //End class metaHelper

/* End of file metaHelper.php */
/* Location: application/core/metaHelper.php */