<?php
/**
 * Crypt.php
 * Provide the 2 main useful functions: secure and release
 * See https://github.com/phpseclib/phpseclib
 * See http://phpseclib.sourceforge.net/
 *
 * Example usage:
 *
 * Retrieve data...
 * $users = $db->get_results( "SELECT email FROM test_users" );
 * foreach( $users as $u )
 * {
 *     echo Crypt::decrypt( $u->email ) .'<br />';
 * }
 * 
 * Insert data into a database...
 * 
 * foreach( range( 501, 1200 ) as $c )
 * {
 *     $email = 'brewwwzy-'. round( $c * 1.2 ) .'@aother.com';
 *     $email = Crypt::encrypt( $email );
 *     $db->insert( 'test_users', array( 'email' => $email ) );
 * }
 *
 * Ref: http://phpseclib.sourceforge.net/
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 24-Nov-2014
 * @package phpseclib
 **/
class Crypt {
    
    static $cipher = null;
    
    public function __construct()
    {
        if( is_null( self::$cipher ) )
        {
            self::init();   
        }
    }
    
    private static function init()
    {
        require_once( realpath( __DIR__ ) . SEP . 'phpseclib'. SEP . 'autoload.php' );
        self::$cipher = new Crypt_AES();
        self::$cipher->setKey( E_ST );
    }
    
    public static function encrypt( $data )
    {
        if( empty( $data ) )
        {
            return '';
        }
        new Crypt();
        return self::$cipher->encrypt( $data );
    }
    
    public static function decrypt( $data )
    {
        if( empty( $data ) )
        {
            return '';
        }
        new Crypt();
        return self::$cipher->decrypt( $data );
    }
}