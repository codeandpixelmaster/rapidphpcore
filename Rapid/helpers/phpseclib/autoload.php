<?php
/**
 * autoload.php
 * Make sure crypt stuff autoloads as needed for encryption
 * Ref: http://phpseclib.sourceforge.net/
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 24-Nov-2014
 * @package phpseclib
 **/
set_include_path( implode( PATH_SEPARATOR, array(
    dirname(__FILE__) . '/phpseclib/',
    dirname(__FILE__) . '/',
    get_include_path(),
) ) );

require_once 'Crypt/Random.php';
function phpseclib_autoload( $class )
{
    $file = str_replace('_', '/', $class) . '.php';
    if (phpseclib_resolve_include_path($file)) {
        // @codingStandardsIgnoreStart
        require $file;
        // @codingStandardsIgnoreEnd
    }
}
spl_autoload_register( 'phpseclib_autoload' );