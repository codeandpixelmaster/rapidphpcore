<?php
/**
 * less-config.php
 * provides initial support and inclusion of less for PHP
 * https://github.com/oyejorge/less.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 01-Oct-2014
 * @package RapidPHPMe
 **/

class LessHelper {

    /**
     * Function to simplify addition or mgmt of multiple files for less usage
     * If caching is used, will make a cachedir in whichever directory being used
     *
     * Automatically initializes Less classes
     * Example usage:
     * $files = array(
     *     'css/normalize.css', 
     *     'css/foundation.css', 
     *     'css/font-awesome.css', 
     *     'css/style.css'
     * );
     * echo LessHelper::less_cache( $files, 'css', true );
     * Will output:
     * <link rel="stylesheet" type="text/css" src="http://yoursite.com/assets/css/cache/your-lessfilehere.css">
     *
     * @access public
     * @param array $files
     * @param string $cache_dir (i.e. 'css')
     * @param bool $cache
     * @return mixed
     */
    public static function less_cache( $files = array(), $cache_dir = '', $cache = false )
    {
        
        Less_Autoloader::register();
        
        if( empty( $files ) )
        {
            return '';
        }

        $extended_path = $cache_dir . SEP . 'cache';

        $this_cache_dir = ASSETS_ROOT . $extended_path;
        if( !is_dir( $this_cache_dir ) )
        {
            mkdir( $this_cache_dir );
        }

        $options = array(
            'compress' => true, 
            'cache_dir' => $this_cache_dir
        );

        $parser = new Less_Parser( $options );
        $for_cache = array();
        foreach( $files as $key => $file )
        {
            if( $cache === true )
            {
                $for_cache[ASSETS_ROOT . $file] = ASSETS . dirname( $file );
            }
            else
            {
                try {
                    $parser->parseFile( ASSETS_ROOT . $file, ASSETS . dirname( $file ) );
                } catch( Exception $e ) {
                    trigger_error( 'Less Parser error: ' . $e->getMessage() );
                }   
            }
        }

        if( $cache === true )
        {
            $css_file_name = Less_Cache::Get( $for_cache, $options );
            return '<link rel="stylesheet" type="text/css" href="'.ASSETS . $extended_path . SEP . $css_file_name.'">';
        }
        else
        {
            $css = $parser->getCss();
        }

        return $css;
    }
       
}