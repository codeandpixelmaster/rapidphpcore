<?php
/**
 * Forms.php
 * Form handling
 * Form return value processing
 *
 * @version 1.0
 * @date 11 Aug 2012
 * @updated 01-Jun-2013
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** clean()          //cleans previously sanitized values for normalized output
 ** filter()         //sanitizes user data PRE mysqli insertion
 ** recall()         //recalls and outputs text and selections on form values on $_POST
 ** validate()       //outputs as echo values within shortcode conditionals that would otherwise fail
 ** valid_email()    //validates email addresses
 ** valid_url()      //validates URIs
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );

class Forms 
{
    /**
     * @type
     */
    private $clean;

    /**
     * @type
     */
    public $filter;


    /**
     * Function to clean values for normalized output
     * @access public
     * @param string $text
     * @return string $text
     */
    public function clean( $text )
    {
        $this->text = html_entity_decode( $text, ENT_QUOTES, 'UTF-8' );
        $this->text = urldecode( $this->text );
        $this->text = nl2br( stripslashes( $this->text ) );
        return $this->text;
    }


    /**
     * Sanitize user data PRE mysqli insertion
     *
     * @access public
     * @param string, array
     * @return string, array
     *
     */
    public function filter( $data )
    {
        if( !is_array( $data ) )
        {
            $this->data = addslashes( $data );
            $thisdata = trim( htmlentities( $this->data, ENT_QUOTES, 'UTF-8', false ) );
        }
        else
        {
            //Self call function to sanitize array data
            $this->data = array_map( array( 'Forms', 'filter' ), $data );
        }

        return $this->data;
    }
    
    
    /**
     * Function to recall and output text and selections on form values
     * Example usage: <input type="hidden" name="myinput" value="<?php recall('myinput'); ?>" />
     * Example usage: <input type="checkbox" name="mycheckbox" id="mycheckbox"<?php recall('mycheckbox', 'checkbox'); ?> />
     * Example usage (select menu): 
     *  <select name="selectmenu">
     *      <option value="option1"<?php recall('selectmenu', 'select', 'option1'); ?>>Option 1</option>
     *      <option value="option2"<?php recall('selectmenu', 'select', 'option2'); ?>>Option 2</option>
     *  </select>
     */
    /**
     * Function to recall and output text and selections on form values
     * Example usage: <input type="hidden" name="myinput" value="<?php recall('myinput'); ?>" />
     * Example usage: <input type="checkbox" name="mycheckbox" id="mycheckbox"<?php recall('mycheckbox', 'checkbox'); ?> />
     * Example usage (select menu): 
     *  <select name="selectmenu">
     *      <option value="option1"<?php recall('selectmenu', 'select', 'option1'); ?>>Option 1</option>
     *      <option value="option2"<?php recall('selectmenu', 'select', 'option2'); ?>>Option 2</option>
     *  </select>
     */
    public function recall( $field_name, $type = 'text', $select_value = '', $echo = true )
    {
        if( isset( $_POST ) && !empty( $_POST ) )
        {
            switch( $type )
            {
                //Text inputs
                case 'text':
                    echo ( !empty( $_POST[$field_name] ) ) ? $this->filter( $_POST[$field_name] ) : '';
                break;

                //Select menus
                case 'select':
                    if( !empty( $_POST[$field_name] ) )
                    {
                        if( $select_value == $_POST[$field_name] )
                        {
                            if( $echo )
                            {
                                echo ' selected="selected"';
                            }
                            else
                            {
                                return ' selected="selected"';
                            }
                        }
                    }
                break;

                //Multi select menus
                case 'multiselect':
                    if( !empty( $_POST[$field_name] ) )
                    {
                        $fieldsum = count( $_POST[$field_name] );
                        for( $i = 0; $i < $fieldsum; $i++ )
                        {
                            if( $_POST[$field_name][$i] == $select_value )
                            {
                                if( $echo )
                                {
                                    echo ' selected="selected"';   
                                }
                                else
                                {
                                    return ' selected="selected"';
                                }
                            }
                        }
                    }
                break;

                case 'checkbox':
                    if( !empty( $_POST[$field_name] ) )
                    {
                        $fieldsum = count( $_POST[$field_name] );
                        for( $i = 0; $i < $fieldsum; $i++ )
                        {
                            if( $_POST[$field_name][$i] == $select_value )
                            {
                                if( $echo )
                                {
                                    echo ' checked="checked"';   
                                }
                                else
                                {
                                    return ' checked="checked"';
                                }
                            }
                        }
                    }
                break;

                //Radio buttons
                case 'radio':
                    echo ( !empty( $_POST[$field_name] ) ) ? ' checked="checked"' : '';
                break;

                //Defaults to text
                default:
                    echo ( !empty( $_POST[$field_name] ) ) ? $this->filter( $_POST[$field_name] ) : '';
                break;
            }
        }
    }
    
    
    /**
     * Function value to output as echo values within shortcode conditionals that would otherwise fail
     * @access public
     * @param mixed $value
     * @return mixed $value
     */
    public function validate( $value )
    {
        echo $value;
    }
    
    
    /**
     * Validate email addresses
     * @access public
     * @param string $address
     * @return bool
     */
    public function valid_email( $address )
    {
        return filter_var( $address, FILTER_VALIDATE_EMAIL ) ? TRUE : FALSE;
    }
    
    
    /**
     * Validate web addresses
     * @access public
     * @param string $address
     * @return bool
     */
    public function valid_url( $address )
    {
        return filter_var( $address, FILTER_VALIDATE_URL ) ? TRUE : FALSE;
    }
       
} //end class Forms

/* End of file Forms.php */
/* Location: application/core/Forms.php */