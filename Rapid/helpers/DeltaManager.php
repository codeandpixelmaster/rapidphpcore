<?php
/**
 * DeltaManager.php
 *
 * @version 1.0
 * @date 9/24/15 7:53 AM
 * @package rapidCore
 */

class DeltaManager extends DatabaseUtility {

	public $delta_directory;

	private $delta_table = 'delta_actions';

	private $user = '';

	private $environment;

	public function __construct( $directory = null )
	{
		parent::__construct();
		if( is_null( $directory ) )
		{
			$this->delta_directory = ASSETS_ROOT . 'files/deltas';
		}

		if( !is_dir( $this->delta_directory ) )
		{
			mkdir( $this->delta_directory );
		}

		if( !$this->db->table_exists( $this->delta_table ) )
		{
			$this->init();
		}

		if( null !== session( 'user_id' ) )
		{
			$this->user = session( 'user_id' );
		}

		//Determine the environment
		$this->environment = !defined( 'ENVIRONMENT' ) ? $_SERVER['HTTP_HOST'] : ENVIRONMENT;
	}


	public function runDeltas()
	{
		$response = array();
		$dir = glob( $this->delta_directory .'/*.sql' );
		foreach( $dir as $file )
		{
			$filename = basename( $file );

			//Check to see if we've run this file already
			$exists = $this->db->num_rows( "SELECT COUNT(id) FROM delta_actions WHERE file = ? AND environment = ? LIMIT 1", array( $filename, $this->environment ) );
			$response[] = 'Checking '. $filename .' for existing in '.$this->environment.', '. $exists .' rows found';
			if( $exists == 0 )
			{
				$response[] = $filename. ' not found in db, running';
				$data = file_get_contents( $file );
				$data = array_filter( explode( ';', $data ) );
				if( empty( $data ) )
				{
					$response[] = 'No lines found in file, skipping '. $filename;
					continue;
				}
				foreach( $data as $run )
				{
					try {
						$this->db->query( $run, array() );
						$message = 'Ran: ' . htmlentities( $run ) .' from file '. $filename .' on '. $this->environment;
					} catch( Exception $e ) {
						$message = $e->getMessage();
					}

					$response[] = $message;
				}

				$filedata = array(
					'environment' => $this->environment,
					'file' => $filename,
					'run' => 'NOW()',
					'user' => $this->user,
				);
				$this->db->insert( $this->delta_table, $filedata );
			}
		}
		return $response;

	}

	private function init()
	{
		$this->add( $this->delta_table )
			->column( 'id', 'int(11)', 'DEFAULT NULL', true )
			->column( 'environment', 'varchar(220)', 'DEFAULT NULL' )
			->column( 'file', 'varchar(220)', 'DEFAULT NULL' )
			->column( 'run', 'timestamp', 'NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP' )
			->column( 'user', 'int(11)' )
			->run();
	}


} //end DeltaManager