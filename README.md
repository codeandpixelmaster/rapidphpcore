RapidPHPMe
===============

A super slim PHP framework for rapid development.


#Changelog

- 25-Dec-2016: Updated html-helper docblocks.  Updated html-helper functions for get, post, session, cookie retrieval to include the ability to pass default values.  Added getParameter() function to html-helper to retrieve only $_GET or $_POST vals
- 15-Nov-2016: Updated ConfigLoader to have time constants, and make sure whoops exists prior to failure
- 01-Nov-2016: Updated EventLogger to accept an override param which disables check to see if record exists, updated template files to initialize parent constructor when extending Control
- 12-Oct-2016: Updated Paginate::getUIPagination to allow output with limited number of page links
- 1.1.1, 31-Aug-2016: Added ability to use closures in add_output method
- 1.1.1, 04-Jul-2016: Updated Dotenv env. handling
- 1.1, 30-Jun-2016: Added Dotenv for environmental variable usage for security